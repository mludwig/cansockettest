//============================================================================
// Name        : peak-driver-test.cpp
// Author      : mludwig
// Version     :
// Copyright   : Your copyright notice
// Description : very basic test of peak_usb driver with peak PCAN-USB Pro FD module
//============================================================================

//#include <iostream>
#include <errno.h>
//#include <fstream>
#include <iostream>

#include <unistd.h>

//#include <time.h>
#include <string.h>
#include <sys/time.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <sys/socket.h>
#include <sys/ioctl.h>
//#include <sys/uio.h>
#include <net/if.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>

#include <libsocketcan.h>

using namespace std;

void mysleep_ms( int ms )
{
	struct timespec tim, tim2;
	tim.tv_sec = (int) (ms / 1000);
	tim.tv_nsec = ms * 1000000;
	if(nanosleep(&tim , &tim2) < 0 ) {
		cout << "Waiting failed (nanosleep)";
	}
}


std::string CanModuleerrnoToString()
{
	const int max_len = 1000;
	char buf[max_len];
#ifdef _WIN32
	strerror_s(buf, max_len - 1, errno);
	return std::string(buf);
#else
	return std::string(strerror_r(errno, buf, max_len - 1));
#endif
}

void printFrameData( struct can_frame f ){
	for ( int i = 0 ; i < f.can_dlc; i++ ){
		cout << ( int ) f.data[i];
	}
	cout << endl;
}


/** according to
 * https://www.can-cia.org/fileadmin/resources/documents/proceedings/2012_kleine-budde.pdf
 */
int main() {
	cout << "version " << __DATE__ << " " << __TIME__ << " peak driver test" << endl;
	//	int returnCode = can_do_stop(m_channelName.c_str());
	struct timeval _dstart, _dstop;
	struct timezone _tz;

	struct ifreq ifr0;
	struct sockaddr_can addr0;
	memset(&ifr0, 0x0, sizeof(ifr0));
	memset(&addr0, 0x0, sizeof(addr0));

	struct ifreq ifr1;
	struct sockaddr_can addr1;
	memset(&ifr1, 0x0, sizeof(ifr1));
	memset(&addr1, 0x0, sizeof(addr1));

	struct can_frame frame0;
	memset(&frame0, 0x0, sizeof(frame0));
	int sock0;

	struct can_frame frame1;
	memset(&frame1, 0x0, sizeof(frame1));
	int sock1;

	sock0 = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if (sock0 < 0) {
		cout << "Error while opening socket: " << CanModuleerrnoToString();
		return -1;
	}
	cout << __FILE__ << " " << __LINE__ << " socket opened: sock0= " << sock0 << endl;

	sock1 = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if (sock1 < 0) {
		cout << "Error while opening socket: " << CanModuleerrnoToString();
		return -1;
	}
	cout << __FILE__ << " " << __LINE__ << " socket opened: sock1= " << sock1 << endl;

	/**
	 *  convert interface string "can0" into interface index
	 */
	string port0 = "can0";
	strcpy(ifr0.ifr_name, port0.c_str() );
	cout << __FILE__ << " " << __LINE__ << " setup " << port0 << endl;
	ioctl( sock0, SIOCGIFINDEX, &ifr0 );

	/* setup address for bind */
	addr0.can_ifindex = ifr0.ifr_ifindex;
	addr0.can_family = PF_CAN ;

	/* bind socket to the can0 interface */
	cout << __FILE__ << " " << __LINE__ << " binding0... " << endl;
	bind( sock0, (struct sockaddr *)&addr0, sizeof(addr0));

	/**
	 *  convert interface string "can1" into interface index
	 */
	string port1 = "can1";
	strcpy(ifr1.ifr_name, port1.c_str() );
	cout << __FILE__ << " " << __LINE__ << " setup " << port1 << endl;
	ioctl( sock1, SIOCGIFINDEX, &ifr1 );

	/* setup address for bind */
	addr1.can_ifindex = ifr1.ifr_ifindex;
	addr1.can_family = PF_CAN ;

	/* bind socket to the can0 interface */
	cout << __FILE__ << " " << __LINE__ << " binding1... " << endl;
	bind( sock1, (struct sockaddr *)&addr1, sizeof(addr1));


	/* first fill, then send the CAN frame */
	frame0.can_id = 0x23;
	frame0.can_dlc = 8;
	frame1.can_id = 0x23;
	frame1.can_dlc = 8;
	// strcpy((char *)frame0.data, "11223344" );
	for ( int i = 0; i < frame0.can_dlc; i++ ){
		frame0.data[ i ] = i;
		frame1.data[ i ] = 7 - i;
	}
	// strcpy((char *)frame1.data, "55667788" );

	int count = 100;

	gettimeofday( &_dstart, &_tz);

	cout << __FILE__ << " " << __LINE__ << " sending " << count << " frames" << endl;
	for ( int i = 0; i < count; i++ ){
		if ( i % 50 == 0 ){
			cout << i << ". port0 "; printFrameData( frame0 );
		}
		write( sock0, &frame0, sizeof(frame0));

		if ( i % 50 == 0 ) {
			cout << i << ". port1 "; printFrameData( frame1 );
		}
		write( sock1, &frame1, sizeof(frame1));

		mysleep_ms( 10 );
	}
	gettimeofday( &_dstop, &_tz);
	double _measDelay = (double) ( _dstop.tv_sec - _dstart.tv_sec) * 1000 + (double) ( _dstop.tv_usec - _dstart.tv_usec) / 1000.0;
	cout << __FILE__ << " " << __LINE__ << " took " << _measDelay << " ms for " << count << " frames" << endl;

	close( sock0 );
	close( sock1 );

#if 0
	struct ifreq ifr;
	memset(&ifr.ifr_name, 0, sizeof(ifr.ifr_name));
	if (m_channelName.length() > sizeof ifr.ifr_name-1)
	{
		MLOG(ERR,this) << "Given name of the port0 exceeds operating-system imposed limit";
		return -1;
	}
	strncpy(ifr.ifr_name, m_channelName.c_str(), sizeof(ifr.ifr_name)-1);

	if (ioctl(sock0, SIOCGIFINDEX, &ifr) < 0)
	{
		MLOG(ERR,this) << "ioctl SIOCGIFINDEX failed. " << CanModuleerrnoToString();
		return -1;
	}

	can_err_mask_t err_mask = 0x1ff;
	if( setsockopt(sock0, SOL_CAN_RAW, CAN_RAW_ERR_FILTER, &err_mask, sizeof err_mask) < 0 )
	{
		MLOG(ERR,this) << "setsockopt() failed: " << CanModuleerrnoToString();
		return -1;
	}

	struct sockaddr_can socketAdress;
	socketAdress.can_family = AF_CAN;
	socketAdress.can_ifindex = ifr.ifr_ifindex;

	if (::bind(sock0, (struct sockaddr *)&socketAdress, sizeof(socketAdress)) < 0)
	{
		MLOG(ERR,this) << "While bind(): " << CanModuleerrnoToString();
		return -1;
	}

	// Fetch initial state of the port0
	if (int ret=can_get_state( m_channelName.c_str(), &m_errorCode ))
	{
		m_errorCode = ret;
		MLOG(ERR,this) << "can_get_state() failed with error code " << ret << ", it was not possible to obtain initial port0 state";
	}

	updateInitialError();


#endif
	return 0;
}
